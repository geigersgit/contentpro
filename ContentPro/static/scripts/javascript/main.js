"use strict";

var homePage = (function(){

  var tabBar, searchButton, dropDown;

  var getDomElements = function(){
    tabBar = document.getElementById("tabBar");
    searchButton = document.getElementById("sb_search");
    dropDown = document.getElementById("dropdown-menu");
  };

  var assignActionListeners = function(){

    tabBar.addEventListener("click", function(event){
      if(event.target != this){
        var tabs = tabBar.getElementsByClassName("tablinks");
        for(var i = 0; i < tabs.length; i++){
          tabs[i].className = tabs[i].className.replace(" active", "");
        }
      }
      event.target.className += " active";
    }, true);

    searchButton.addEventListener("click", function(){
      var url = document.getElementById("searchBar").value;
      document.getElementById("iFrameWindow").src = url;
    });

    dropDown.addEventListener("click", function(event){
      var url = event.target.getAttribute("id");
      document.getElementById("iFrameWindow").src = url;
      document.getElementById("searchBar").value = url;
    });

  };

  var init = function (){
    getDomElements();
    assignActionListeners();
  };

  return {
    init: init
  };

})();

homePage.init();
