from flask import Flask, render_template
import os

os.putenv('FLASK_APP', 'ContentPro')

app = Flask(__name__)
# app.config['FLASK_APP'] = 'ContentPro'

import ContentPro.views
