from ContentPro import app
from flask import render_template

@app.route("/")
@app.route("/index")
def index():

    link1 = {'name':'SNMP Trap Basics',
    'url':'http://www.dpstele.com/snmp/trap-basics1.1_c.php'}
    link2 = {'name':'OID Network Elements',
    'url':'http://www.dpstele.com/snmp/what-does-oid-network-elements.php'}
    link3 = {'name':'Main Page',
    'url':'http://www.dpstele.com/index.php'}
    link4 = {'name':'SNMP Version Difference',
    'url':'http://www.dpstele.com/snmp/v1-v2c-v3-difference.php'}
    link5 = {'name':'Microwave Monitoring',
    'url':'http://www.dpstele.com/network-monitoring/microwave/index.php'}
    link6 = {'name':'TCP Addressing',
    'url':'http://www.dpstele.com/dps/protocol/2001/may-jun/ip-addressing-tcp-dynamic-static.php'}
    link7 = {'name':'SNMP Management Information',
    'url':'http://www.dpstele.com/snmp/management-information-base.php'}
    link8 = {'name':'SCADA dcs-vs',
    'url':'http://www.dpstele.com/scada/dcs-vs.php'}
    link9 = {'name':'SCADA How systems Work',
    'url':'http://www.dpstele.com/scada/how-systems-work.php'}
    link10 = {'name':'SCADA Fundamentals',
    'url':'http://www.dpstele.com/scada/introduction-fundamentals-implementation.php'}
    topTenLinks = {'link1':link1, 'link2':link2, 'link3':link3,
    'link4':link4,'link5':link5, 'link6':link6, 'link7':link7,
    'link8':link8,'link9':link9, 'link10':link10}

    tabPages = {"pages/tab1.html", "pages/tab2.html",
    "pages/tab3.html", "pages/tab4.html"};

    return render_template('index.html', links=topTenLinks)
