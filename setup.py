from setuptools import setup

setup(
    name='ContentPro',
    packages=['ContentPro'],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)
